const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

//mongoDB connection
const uri = process.env.MongoDB;
mongoose.connect(uri,{useNewUrlParser:true,useCreateIndex:true,useUnifiedTopology: true});
const connection = mongoose.connection;
connection.once('open', ()=>{
    console.log("connected to MongoDB");
})

//routes
const studyRoutes = require("./routes/study");
app.use('/studies',studyRoutes);

const workRoutes = require("./routes/work");
app.use('/work',workRoutes);

//start server
app.listen(port, ()=>{
    console.log(`Server running on port : ${port}`);
})

/**
    https://medium.com/@beaucarnes/learn-the-mern-stack-by-building-an-exercise-tracker-mern-tutorial-59c13c1237a1

    https://www.digitalocean.com/community/tutorials/getting-started-with-the-mern-
    
    https://mongoosejs.com/docs/guide.html#models
*/