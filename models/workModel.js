const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const workSchema = new Schema({
    company :{
        type : String,
        required: true,
        trim: true,
        minlength: 3
    },
    
    jobTitle :{
        type : String,
        required: true,
        trim: true,
    },
    
    description :{
        type: String,
        required: true,
        trim: true
    },

    startDate :{
        type : Date,
        required: true,
    },
    endDate :{
        type : Date,
    },

    skills : [{
        type: String
    }],
});

const Study = mongoose.model('work',workSchema);

module.exports = Study;