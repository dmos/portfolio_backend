const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const studySchema = new Schema({
    curseName :{
        type : String,
        required: true,
        unique: true,
        trim: true,
        minlength: 3
    },
    
    place :{
        type : String,
        required: true,
        trim: true,
    },
    
    description :{
        type: String,
        required: true,
        trim: true
    },

    startDate :{
        type : Date,
        required: true,
    },
    endDate :{
        type : Date,
    },

    skills: [{
        type: String
    }],
});

const Study = mongoose.model('study',studySchema);

module.exports = Study;