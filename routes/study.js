const router = require('express').Router();

let Study = require('../models/studyModel')

router.route('/').get( (req,res) => {
    Study.find().sort({startDate: -1})
    .then( studies => res.json(studies))
    .catch(err => res.status(400).json('Error ' +err));
});

router.route('/new').post((req,res) => {
    const curseName = req.body.curseName;
    const place = req.body.place;
    const description = req.body.description;
    const startDate = Date.parse(req.body.startDate);

    const endDate = null;
    if (req.body.endDate != null){
        endDate = Date.parse(req.body.endDate);
    }
    let skills = null
    if (req.body.skills != null) {
        skills = req.body.skills;
    }

    const newStudy = new Study(
        {curseName,place,description,startDate,endDate,skills}
    );

    newStudy.save()
    .then( ()=> res.json('study added!'))
    .catch(err => res.status(400).json('Error ' + err));

});

router.route('/:id').get((req,res)=>{
    Study.findById(req.params.id)
    .then(study => res.json(study))
    .catch(err => res.status(400).json('Error ' + err));
});

router.route('/:id/delete').delete((req, res) => {
    Study.findByIdAndDelete(req.params.id)
        .then(() => res.json('Study deleted.'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id/update').put((req,res)=>{
    Study.findById(req.params.id)
    .then(study=>{
        study.curseName = req.body.curseName;
        study.place = req.body.place;
        study.description = req.body.description;
        study.startDate = Date.parse(req.body.startDate);
        if (req.body.endDate != null) {
            study.endDate = Date.parse(req.body.endDate);
        }
        if (req.body.skills != null) {
            skills = req.body.skills;
        }

        study.save()
            .then(() => res.json('Study updated!'))
            .catch(err => res.status(400).json('Erro: ' + err));
    })
    .catch (err => res.status(400).json('Error: ' + err));
});


module.exports = router