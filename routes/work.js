const router = require('express').Router();

let Work = require('../models/workModel')

router.route('/').get( (req,res) => {
    Work.find().sort({ startDate: -1 })
    .then( works => res.json(works))
    .catch(err => res.status(400).json('Error ' +err));
});

router.route('/new').post((req,res) => {
    const company = req.body.company;
    const jobTitle = req.body.jobTitle;
    const description = req.body.description;
    const startDate = Date.parse(req.body.startDate);

    const endDate = null;
    if (req.body.endDate != null){
        endDate = Date.parse(req.body.endDate);
    }
    let skills = null
    if(req.body.skills != null){
        skills = req.body.skills;
    }

    const newWork = new Work(
        { company, jobTitle, description, startDate, endDate, skills}
    );

    newWork.save()
    .then( ()=> res.json('work added!'))
    .catch(err => res.status(400).json('Error ' + err));

});

router.route('/:id').get((req,res)=>{
    Work.findById(req.params.id)
    .then(work => res.json(work))
    .catch(err => res.status(400).json('Error ' + err));
});

router.route('/:id/delete').delete((req, res) => {
    Work.findByIdAndDelete(req.params.id)
        .then(() => res.json('Work deleted.'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id/update').put((req,res)=>{
    Work.findById(req.params.id)
    .then(work=>{
        work.company = req.body.company;
        work.jobTitle = req.body.jobTitle;
        work.description = req.body.description;
        work.startDate = Date.parse(req.body.startDate);
        if (req.body.endDate != null) {
            work.endDate = Date.parse(req.body.endDate);
        }
        if (req.body.skills != null) {
            skills = req.body.skills;
        }

        work.save()
            .then(() => res.json('Work updated!'))
            .catch(err => res.status(400).json('Erro: ' + err));
    })
    .catch (err => res.status(400).json('Error: ' + err));
});


module.exports = router